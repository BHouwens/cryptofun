use rand::{ thread_rng, Rng, OsRng };
use std::ops::{ Shl, BitXor, Rem, Shr };
use ramp::{ Int, RandomInt };

const LARGE_THRESHOLD: usize = 25;


/*-------- PUBLIC FUNCTIONS --------*/


/// Generates a prime number
/// 
/// ### Arguments
/// 
/// * `bitlength` - The bit length of the number
pub fn generate(bitlength: &usize) -> Int {
    let mut generator = thread_rng();

    loop {
        let candidate = generate_random_bigint(&mut generator, bitlength);

        if (bitlength < &LARGE_THRESHOLD && is_small_prime(&candidate)) || 
           (bitlength >= &LARGE_THRESHOLD && is_large_prime(&candidate)) {
            return candidate;
        }
    }
}

/// Generates a prime number that is safe for discrete log crypto
/// 
/// ### Arguments
/// 
/// * `bitlength` - Bit length of prime number
pub fn generate_discrete_log_prime(bitlength: &usize) -> Int {
    loop {
        let candidate = generate(bitlength);

        if is_discrete_log_safe(&candidate) {
            return candidate;
        }
    }
}

/// Checks for prime number safety by ensuring that 
/// "p" is a Sophie Germain prime (that is, "2p + 1" is also prime)
/// 
/// ### Arguments
/// 
/// * `candidate` - Candidate prime, the "p" in above equation
fn is_discrete_log_safe(candidate: &Int) -> bool {
    let two = Int::one() + Int::one();
    let q = candidate.shr(1) - Int::one();

    if q.clone().rem(two) != Int::zero() {
        return is_large_prime(&q);
    }

    false
}

/// Gets the modular inverse for provided parameters using Extended Euclidean
/// 
/// ### Arguments
/// 
/// * `a` - Value to apply EE to
/// * `modulus` - Modulus for calculation
pub fn modular_inverse(a: &Int, modulus: &Int) -> Int {
    let mut mn = (modulus.clone(), a.clone());
    let mut xy = (Int::zero(), Int::one());
 
    while mn.1 != Int::zero() {
        xy = (xy.1.clone(), xy.0.clone() - (mn.0.clone() / mn.1.clone()) * xy.1.clone());
        mn = (mn.1.clone(), mn.0.clone() % mn.1.clone());
    }
 
    while xy.0 < Int::zero() {
        xy.0 = xy.0.clone() + modulus.clone();
    }

    xy.0
}


/// Gets the modular inverse for provided parameters using Extended Euclidean,
/// with a signed integer parameter expectation. Useful for Jacobian coordinate 
/// normalization
/// 
/// ### Arguments
/// 
/// * `a` - Value to apply EE to
/// * `modulus` - Modulus for calculation
pub fn modular_inverse_int(a: &Int, modulus: &Int) -> Int {
    let mut mn = (modulus.clone(), a.clone());
    let mut xy = (Int::zero(), Int::one());
 
    while mn.1 != Int::zero() {
        xy = (xy.1.clone(), xy.0.clone() - (mn.0.clone() / mn.1.clone()) * xy.1.clone());
        mn = (mn.1.clone(), mn.0.clone() % mn.1.clone());
    }
 
    while xy.0 < Int::zero() {
        xy.0 = xy.0.clone() + modulus.clone();
    }

    xy.0
}

 
/// Generates an optimised large number for primality testing
/// 
/// ### Arguments
/// 
/// * `generator` - Random number generator
/// * `bitlength` - Bit length for number
pub fn generate_random_bigint(generator: &mut Rng, bitlength: &usize) -> Int {
    let candidate: Int = generator.gen_int(bitlength - 1);
    let shifted_candidate = candidate.shl(1);
    let final_candidate = shifted_candidate.bitxor(Int::one());

    final_candidate
}


/*-------- PRIVATE FUNCTIONS --------*/


/// Full, efficient check whether large candidate is prime
/// 
/// ### Arguments
/// 
/// * `candidate` - Candidate to check
fn is_large_prime(candidate: &Int) -> bool {
    if !fermat_little(candidate) {
        return false;
    }

    if !miller_rabin(candidate, 3) {
        return false;
    }

    true
}
 
/// Full check whether small candidate is prime
/// 
/// ### Arguments
/// 
/// * `candidate` - Candidate to check
fn is_small_prime(candidate: &Int) -> bool {
    let cast = candidate.clone().to_f64();

    if cast < 2.0 {
        return false;

    } else if cast == 2.0 {
        return true;

    } else {
        let sqrt = cast.sqrt().abs();
        let u64_cast = cast as u64;

        for i in 2..(sqrt as u64) {
            if u64_cast % i == 0 {
                return false;
            }
        }
    }

    true
}
 
/// Checks whether a candidate is definitely composite
/// based on Fermat's little theorem
/// 
/// ### Arguments
/// 
/// * `candidate` - Candidate to check
fn fermat_little(candidate: &Int) -> bool {
    let mut generator = thread_rng();
    let random: Int = generator.gen_uint_below(candidate);
    let result = random.pow_mod(&(candidate - Int::one()), candidate);

    result == Int::one()
}

/// Checks whether candidate is prime via Miller-Rabin test.
/// 3 iterations is considered secure at an error probability of 2^80
/// 
/// ### Arguments
/// 
/// * `candidate` - Candidate to check
/// * `iterations` - Number of iterations to perform
fn miller_rabin(candidate: &Int, iterations: usize) -> bool {
    let (s, d) = greatest_2_divisor(candidate);
    let one = Int::one();
    let two = &one + &one;
    let mut generator = thread_rng();

    for _ in 0..iterations {
        let basis = generator.gen_int_range(&two, &(candidate - &two));
        let mut y = basis.pow_mod(&d, candidate);

        if y == one || y == (candidate - &one) {
            continue;
        } else {
            for _ in 0..s {
                y = y.pow_mod(&two, candidate);

                if y == one {
                    return false;
                } else if y == candidate - &one {
                    break;
                }
            }

            return false;
        }
    }

    true
}

/// Util function for Miller-Rabin test
/// 
/// ### Arguments
/// 
/// * `num` - Number to use
fn greatest_2_divisor(num: &Int) -> (usize, Int) {
    let mut s = 0;
    let mut num = num - Int::one();

    while num.is_even() {
        num = num >> 1;
        s += 1
    }

    (s, num)
}